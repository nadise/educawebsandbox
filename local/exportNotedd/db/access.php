<?php 

/**
 * Version file for component 'local_exportNote'
 * 
 * @package    local_exportNote
 * @copyright  2019 onwards exportNote
 * @developer  Brian kremer (greatwallstudio.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
*/  

$capabilities = array( 
	'local/exportNote:accessquerypractice' => array(
		'riskbitmask'  => RISK_CONFIG | RISK_DATALOSS,
		'captype'      => 'write',
		'contextlevel' => CONTEXT_SYSTEM,
		'archetypes'   => array(
		'admin'    => CAP_ALLOW
		)
	)
);