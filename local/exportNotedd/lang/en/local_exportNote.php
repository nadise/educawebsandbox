<?php 

/**
 * Version file for component 'local_exportNote'
 * 
 * @package    local_exportNote
 * @copyright  2019 onwards exportNote
 * @developer  Brian kremer (greatwallstudio.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
*/  

$string['pluginname'] = 'exportNote';