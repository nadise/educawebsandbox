<?php

/**
 * Version file for component 'local_exportNote'
 * 
 * @package    local_exportNote
 * @copyright  2019 onwards exportNote
 * @developer  Brian kremer (greatwallstudio.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

//Includes
require_once('../../config.php');
require_once('importNote.php');

//require_once($CFG->libdir . '/adminlib.php');
global $DB;

ini_set('display_errors', 1);
error_reporting(E_ALL);

//Set page object
$PAGE->set_context(context_system::instance());
$url = new moodle_url('/local/exportNote/index.php');
$PAGE->set_url($url);
$PAGE->set_pagelayout('report');
require_login();
$context = context_system::instance();

//Bread crumb trail
$previewnode = $PAGE->navigation->add(get_string('pluginname', 'local_exportNote'), new moodle_url('index.php'), navigation_node::TYPE_CONTAINER);
$previewnode->make_active();
require_capability('local/exportNote:accessquerypractice', $context);

//Navigation and header 
$strpluginname = $SITE->fullname . ' query practice - index.php';
$PAGE->set_title($strpluginname);
$PAGE->set_heading($strpluginname);
echo $OUTPUT->header();
echo html_writer::nonempty_tag('h3', 'exportNote');

echo html_writer::nonempty_tag('p', 'Page to practice writing queries... find moodle/local/exportNote/index.php, and edit it! ');

// ********** ********** SECTION 1 ********** ********** 

//example of get_records_sql
echo '<hr><p>Example of get_records_sql() function</p>';

//put query in a variable
//$sql = "SELECT * FROM mdl_grade_grades WHERE 180";
//$sql = "SELECT * FROM mdl_user WHERE username LIKE \"gen%\"";
//$sql = "SELECT username FROM mdl_course WHERE id=21";

$courseid = 21; // ID du cours à récupérer
$users = array(); // tableau pour stocker les utilisateurs

// Requête pour récupérer les utilisateurs inscrits au cours
$sql = "SELECT u.*
        FROM mdl_user u
        JOIN mdl_user_enrolments ue ON u.id = ue.userid
        JOIN mdl_enrol e ON ue.enrolid = e.id
        WHERE e.courseid = ?";
$params = array($courseid);

// Exécuter la requête
$users_result = $DB->get_records_sql($sql, $params);

// Parcourir les utilisateurs et ajouter leurs informations à $users
foreach ($users_result as $user) {
	$users[] = $user;
}

// Afficher les informations des utilisateurs
foreach ($users as $user) {
	// echo "ID utilisateur : " . $user->id . "<br>";
	echo "Nom complet : " . $user->firstname . " " . $user->lastname . "<br>";
	echo "Adresse email : " . $user->email . "<br>";
	// Ajouter d'autres informations si nécessaire
}

//display the query
echo '<p>Query: <b>' . $sql . '</b></p><hr>';

echo '**********************************************     les notes        ************************************************';



$notesItems = "SELECT u.firstname, u.lastname,
                     gg_lire.finalgrade as note_lire,
                     gg_ecrire.finalgrade as note_ecrire,
                     gg_ecouter.finalgrade as note_ecouter,
                     gg_conversation.finalgrade as 'note_conversation',                     
                     gg_expression.finalgrade as 'note_expression'                     
              FROM mdl_user u
              JOIN mdl_user_enrolments ue ON u.id = ue.userid
              JOIN mdl_enrol e ON ue.enrolid = e.id


              JOIN mdl_grade_items gi_lire ON e.courseid = gi_lire.courseid
              JOIN mdl_grade_items gi_ecrire ON e.courseid = gi_ecrire.courseid
              JOIN mdl_grade_items gi_ecouter ON e.courseid = gi_ecouter.courseid
              JOIN mdl_grade_items gi_conversation ON e.courseid = gi_conversation.courseid
              JOIN mdl_grade_items gi_expression ON e.courseid = gi_expression.courseid

              JOIN mdl_grade_grades gg_lire ON gi_lire.id = gg_lire.itemid AND u.id = gg_lire.userid
              JOIN mdl_grade_grades gg_ecrire ON gi_ecrire.id = gg_ecrire.itemid AND u.id = gg_ecrire.userid
              JOIN mdl_grade_grades gg_ecouter ON gi_ecouter.id = gg_ecouter.itemid AND u.id = gg_ecouter.userid
              JOIN mdl_grade_grades gg_conversation ON gi_conversation.id = gg_conversation.itemid AND u.id = gg_conversation.userid
              JOIN mdl_grade_grades gg_expression ON gi_expression.id = gg_expression.itemid AND u.id = gg_expression.userid


              WHERE e.courseid = 21 
                    AND gi_lire.itemname = 'lire'
                    AND gi_ecouter.itemname = 'écouter'
                    AND gi_ecrire.itemname = 'écrire'
                    AND gi_conversation.itemname = 'conversation'
                    AND gi_expression.itemname = 'expression'
                   ";

$result = $DB->get_records_sql($notesItems);
echo $row;
// Afficher les noms et les notes des utilisateurs
if (!empty($result)) {
    echo '<table>';
    echo '<tr><th>Prénom</th><th>Nom  |</th><th>Note LIRE  |</th><th>Note ÉCOUTER|</th><th>Note ÉCRIRE|</th><th>Note CONVERSATION|</th><th>Note EXPRESSION|</th></tr>';
    foreach ($result as $row) {
        echo '<tr><td>'.$row->firstname.'</td><td>'.$row->lastname.'</td><td>'.$row->note_lire.'</td><td>'.$row->note_ecouter.'</td>
                  <td>'.$row->note_ecrire.'</td><td>'.$row->note_conversation.'</td><td>'.$row->note_expression.'</td>
        </tr>';
    }
    echo '</table>';
} else {
    echo 'Aucun résultat trouvé.';
}
echo $row->firstname;
echo $row->note_ecouter;



/*
// Exécuter la requête SQL pour récupérer les noms et les notes des utilisateurs
$notesLire = "SELECT u.firstname, u.lastname, gg.finalgrade
FROM mdl_user u
JOIN mdl_user_enrolments ue ON u.id = ue.userid
JOIN mdl_enrol e ON ue.enrolid = e.id
JOIN mdl_grade_items gi ON e.courseid = gi.courseid
JOIN mdl_grade_grades gg ON gi.id = gg.itemid AND u.id = gg.userid
WHERE e.courseid = 21 AND gi.itemname = 'lire'
";
$result = $DB->get_records_sql($notesLire);

// Afficher les noms et les notes des utilisateurs
if (!empty($result)) {
    echo '<table>';
    echo '<tr><th>Prénom</th><th>Nom</th><th>Note LIRE</th></tr>';
    foreach ($result as $row) {
        echo '<tr><td>'.$row->firstname.'</td><td>'.$row->lastname.'</td><td>'.$row->finalgrade.'</td></tr>';
    }
    echo '</table>';
} else {
    echo 'Aucun résultat trouvé.';
}
*/

/*
$notes = "SELECT u.firstname, u.lastname, gg.finalgrade
FROM mdl_user u
JOIN mdl_user_enrolments ue ON u.id = ue.userid
JOIN mdl_enrol e ON ue.enrolid = e.id
JOIN mdl_grade_grades gg ON u.id = gg.userid AND e.courseid = gg.courseid
WHERE e.courseid = 21";


echo '<p>Query: <b>' . $notes . '</b></p></hr>';



/*
//send the query to the get_records_sql() function
$results = $DB->get_records_sql($sql);

//display the results of the function call
echo 'Query results: ';
print_r($results);
	
//Show page footer
echo $OUTPUT->footer();