<?php

/**
 * Open LMS framework
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://opensource.org/licenses/gpl-3.0.html.
 *
 * @copyright Copyright (c) 2009 Open LMS (https://www.openlms.net)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU Public License
 * @package local_exportNote
 * @author Mark Nielsen
 */

/**
 * View renderer
 *
 * @author Mark Nielsen
 * @package local_exportNote
 */


 
 //require_once($CFG->libdir . '/adminlib.php'); ==> ca ne marche pas quand c'est decommenté
 
 require_once('../../config.php');
 require_once($CFG->dirroot . '/local/mr/bootstrap.php');
 require_once($CFG->dirroot . '/local/exportNote/jpgraph/src/jpgraph.php');
 require_once($CFG->dirroot . '/local/exportNote/jpgraph/src/jpgraph_radar.php');
 //require_once('index2.php');
 // Inclure les fichiers requis pour utiliser l'API de Moodle
 //require_once(DIR . '/../../config.php');
 require_once($CFG->libdir . '/gradelib.php');
 global $DB;



ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);



// Données à utiliser dans le graphique
$data = array(85, 45, 95, 10, 50);
//$data = array($row->note_lire , $row->note_ecouter , $row->note_conversation , $row->note_expression , $row->note_ecrire);

// Titres à utiliser pour chaque valeur
$titles = array('ecouter', 'lire', 'conversation', 'expression', 'ecrire', 'Valeur 6');


// Créer un objet graphique de 400 x 250 pixels

$graph = new RadarGraph(400, 400);

// Définir l'échelle de l'axe des x
$graph->SetScale("lin", 0, 100);

//couleur du fond du graph
$graph->SetColor(array(50,150,100));
$graph->SetTitles($titles);

// Créer un objet radarplot avec les données
$radarplot = new RadarPlot($data);

// Ajouter le radarplot au graphique
$graph->Add($radarplot);

// Ajouter une légende
$graph->legend->Pos(0.1, 0.1);

// Afficher le graphique
$graph->stroke();





