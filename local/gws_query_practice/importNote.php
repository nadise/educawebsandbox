<?php

/**
 * Version file for component 'local_gws_query_practice'
 * 
 * @package    local_gws_query_practice
 * @copyright  2019 onwards GWS
 * @developer  Brian kremer (greatwallstudio.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

//Includes
require_once('../../config.php');
global $DB;
$context = context_system::instance();




$courseid = 29; // ID du cours à récupérer
$users = array(); // tableau pour stocker les utilisateurs

// Requête pour récupérer les utilisateurs inscrits au cours
$sql = "SELECT u.*
        FROM mdl_user u
        JOIN mdl_user_enrolments ue ON u.id = ue.userid
        JOIN mdl_enrol e ON ue.enrolid = e.id
        WHERE e.courseid = ?";
$params = array($courseid);

// Exécuter la requête
$users_result = $DB->get_records_sql($sql, $params);



//echo '**********************************************     les notes        ************************************************';

$courseid = 29;
function get_notes_items($courseid){
    global $DB;
    
    $sql = "SELECT u.firstname, u.lastname,
                     gg_lire.finalgrade as note_lire,
                     gg_ecrire.finalgrade as note_ecrire,
                     gg_ecouter.finalgrade as note_ecouter,
                     gg_conversation.finalgrade as 'note_conversation',                     
                     gg_expression.finalgrade as 'note_expression'                     
              FROM mdl_user u
              JOIN mdl_user_enrolments ue ON u.id = ue.userid
              JOIN mdl_enrol e ON ue.enrolid = e.id


              JOIN mdl_grade_items gi_lire ON e.courseid = gi_lire.courseid
              JOIN mdl_grade_items gi_ecrire ON e.courseid = gi_ecrire.courseid
              JOIN mdl_grade_items gi_ecouter ON e.courseid = gi_ecouter.courseid
              JOIN mdl_grade_items gi_conversation ON e.courseid = gi_conversation.courseid
              JOIN mdl_grade_items gi_expression ON e.courseid = gi_expression.courseid

              JOIN mdl_grade_grades gg_lire ON gi_lire.id = gg_lire.itemid AND u.id = gg_lire.userid
              JOIN mdl_grade_grades gg_ecrire ON gi_ecrire.id = gg_ecrire.itemid AND u.id = gg_ecrire.userid
              JOIN mdl_grade_grades gg_ecouter ON gi_ecouter.id = gg_ecouter.itemid AND u.id = gg_ecouter.userid
              JOIN mdl_grade_grades gg_conversation ON gi_conversation.id = gg_conversation.itemid AND u.id = gg_conversation.userid
              JOIN mdl_grade_grades gg_expression ON gi_expression.id = gg_expression.itemid AND u.id = gg_expression.userid


              WHERE e.courseid = $courseid 
                    AND gi_lire.itemname = 'lire'
                    AND gi_ecouter.itemname = 'écouter'
                    AND gi_ecrire.itemname = 'écrire'
                    AND gi_conversation.itemname = 'conversation'
                    AND gi_expression.itemname = 'expression'
                   ";


$params = ['courseid' => $courseid];

return $DB->get_records_sql($sql, $params);
}
























/*
// Exécuter la requête SQL pour récupérer les noms et les notes des utilisateurs
$notesLire = "SELECT u.firstname, u.lastname, gg.finalgrade
FROM mdl_user u
JOIN mdl_user_enrolments ue ON u.id = ue.userid
JOIN mdl_enrol e ON ue.enrolid = e.id
JOIN mdl_grade_items gi ON e.courseid = gi.courseid
JOIN mdl_grade_grades gg ON gi.id = gg.itemid AND u.id = gg.userid
WHERE e.courseid = 21 AND gi.itemname = 'lire'
";
$result = $DB->get_records_sql($notesLire);

// Afficher les noms et les notes des utilisateurs
if (!empty($result)) {
    echo '<table>';
    echo '<tr><th>Prénom</th><th>Nom</th><th>Note LIRE</th></tr>';
    foreach ($result as $row) {
        echo '<tr><td>'.$row->firstname.'</td><td>'.$row->lastname.'</td><td>'.$row->finalgrade.'</td></tr>';
    }
    echo '</table>';
} else {
    echo 'Aucun résultat trouvé.';
}
*/

/*
$notes = "SELECT u.firstname, u.lastname, gg.finalgrade
FROM mdl_user u
JOIN mdl_user_enrolments ue ON u.id = ue.userid
JOIN mdl_enrol e ON ue.enrolid = e.id
JOIN mdl_grade_grades gg ON u.id = gg.userid AND e.courseid = gg.courseid
WHERE e.courseid = 21";


echo '<p>Query: <b>' . $notes . '</b></p></hr>';



/*
//send the query to the get_records_sql() function
$results = $DB->get_records_sql($sql);

//display the results of the function call
echo 'Query results: ';
print_r($results);

