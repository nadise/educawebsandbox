<?php

/**
 * Open LMS framework
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://opensource.org/licenses/gpl-3.0.html.
 *
 * @copyright Copyright (c) 2009 Open LMS (https://www.openlms.net)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU Public License
 * @package local_gws_query_practice
 * @author Mark Nielsen
 */

/**
 * View renderer
 *
 * @author Mark Nielsen
 * @package local_gws_query_practice
 */



//require_once($CFG->libdir . '/adminlib.php'); ==> ca ne marche pas quand c'est decommenté

require_once('../../config.php');
require_once($CFG->dirroot . '/local/mr/bootstrap.php');
require_once($CFG->dirroot . '/local/gws_query_practice/jpgraph/src/jpgraph.php');
require_once($CFG->dirroot . '/local/gws_query_practice/jpgraph/src/jpgraph_radar.php');
require_once($CFG->libdir . '/gradelib.php');
require_once('./importNote.php');
//require_once($CFG->libdir . '/adminlib.php');

global $DB;



ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//$data = array(85, 45, 95, 35, 50);
//$titles = array('ecouter', 'lire', 'conversation', 'expression', 'ecrire',);




// Données à utiliser dans le graphique
// Titres à utiliser pour chaque valeur




$notesItems = get_notes_items($courseid);
$data = array();
$titles = array();





foreach ($notesItems as $row) {

    $data[] = intval($row->note_ecouter);    
    $data[] = intval($row->note_lire);
    $data[] = intval($row->note_conversation);
    $data[] = intval($row->note_expression);
    $data[] = intval($row->note_ecrire);
    $titles[] = 'ecouter';
    $titles[] = 'lire';
    $titles[] = 'conversation';
    $titles[] = 'expression';
    $titles[] = 'ecrire';
    
    
    
    // Créer un objet graphique de 400 x 250 pixels
$graph = new RadarGraph(400, 400);

 
// Définir l'échelle de l'axe des x
$graph->SetScale("lin", 0, 100);

//couleur du fond du graph
$graph->SetColor(array(50, 150, 100));
$graph->SetTitles($titles);

// Créer un objet radarplot avec les données
$radarplot = new RadarPlot($data);


// Ajouter le radarplot au graphique
$graph->Add($radarplot);

// Ajouter une légende
$graph->legend->Pos(0.1, 0.1);

// Afficher le graphique
$graph->stroke();
// afficher les données récupérées
}