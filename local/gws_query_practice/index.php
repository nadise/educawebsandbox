<?php

/**
 * Version file for component 'local_gws_query_practice'
 * 
 * @package    local_gws_query_practice
 * @copyright  2019 onwards GWS
 * @developer  Brian kremer (greatwallstudio.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../config.php');
require_once('importNote.php');
global $DB;
//Includes
require_once($CFG->libdir . '/adminlib.php');
require_once($CFG->dirroot . '/local/mr/bootstrap.php');
require_once($CFG->dirroot . '/local/gws_query_practice/jpgraph/src/jpgraph.php');
require_once($CFG->dirroot . '/local/gws_query_practice/jpgraph/src/jpgraph_radar.php');
require_once($CFG->libdir . '/gradelib.php');




ini_set('display_errors', 1);
error_reporting(E_ALL);

//Set page object
$PAGE->set_context(context_system::instance());
$url = new moodle_url('/local/gws_query_practice/index.php');
$PAGE->set_url($url);
$PAGE->set_pagelayout('report');
require_login();
$context = context_system::instance();

//Bread crumb trail
$previewnode = $PAGE->navigation->add(get_string('pluginname', 'local_gws_query_practice'), new moodle_url('index.php'), navigation_node::TYPE_CONTAINER);
$previewnode->make_active();
require_capability('local/gws_query_practice:accessquerypractice', $context);

//Navigation and header 
$strpluginname = $SITE->fullname . ' query practice - index.php';
$PAGE->set_title($strpluginname);
$PAGE->set_heading($strpluginname);
echo $OUTPUT->header();
echo html_writer::nonempty_tag('h3', 'GWS query practice');

echo html_writer::nonempty_tag('p', 'Page to practice writing queries... find moodle/local/gws_query_practice/index.php, and edit it! ');

// ********** ********** SECTION 1 ********** ********** 

//example of get_records_sql
echo '<hr><p>Example of get_records_sql() function</p>';

//put query in a variable
//$sql = "SELECT * FROM mdl_grade_grades WHERE 180";
//$sql = "SELECT username FROM mdl_user ";
//$sql = "SELECT username FROM mdl_course WHERE id=21";

$sql = "SELECT fullname FROM mdl_course WHERE id = 28";

$resultat = $DB->get_records_sql($sql);
print_r($resultat);

//display the query
echo '<p>Query: <b>' . $sql . '</b></p><hr>';



// ********** ********** SECTION 2 ********** **********  

//Query the role table
$roletablerecords = $DB->get_records_select('role', 'id != ?', array('0'), 'id');

echo '<p></p>';

//Make a heading 
echo '<b>id     -     shortname </b><br>'; 
	
//Loop through the query results
foreach ($roletablerecords as $showroletablerecords) {    
    
	//Show role id and shortname
	echo $showroletablerecords->id . '     -     ' . $showroletablerecords->shortname .  '<br>';
}

// ********** ********** ********** ********** ********** 

//Show page footer
echo $OUTPUT->footer();


echo '**********************************************     Info user       ************************************************'.'</br>';

// c'est seulement l'affichage : le sql est dans import.php fichier //

// Parcourir les utilisateurs et ajouter leurs informations à $users
foreach ($users_result as $user) {
    $users[] = $user;
}

// Afficher les informations des utilisateurs
foreach ($users as $user) {
    // echo "ID utilisateur : " . $user->id . "<br>";
    echo "Nom complet : " . $user->firstname . " " . $user->lastname . "<br>";
    echo "Adresse email : " . $user->email . "<br>";
    // Ajouter d'autres informations si nécessaire
}





echo '**********************************************     les notes        ************************************************<br>';

// Afficher les noms et les notes des utilisateurs

$courseid = 29; 

$result = get_notes_items($courseid); 

// Afficher les noms et les notes des utilisateurs 

foreach ($result as $row) { 

    echo $row->firstname . ' ' . $row->lastname . ': <br>'; 

    echo 'lire: ' . intval($row->note_lire) . ', <br>'; 

    echo 'écrire: ' . intval($row->note_ecrire) . ', <br>'; 

    echo 'écouter: ' . intval($row->note_ecouter) . ', <br>'; 

    echo 'conversation: ' . intval($row->note_conversation) . ', <br>'; 

    echo 'expression: ' . intval($row->note_expression) . '<br><br><br>'; 

} /*

if (!empty($result)) {
    echo '<table>';
    echo '<tr><th>Prénom</th><th>Nom  |</th><th>Note LIRE  |</th><th>Note ÉCOUTER|</th><th>Note ÉCRIRE|</th><th>Note CONVERSATION|</th><th>Note EXPRESSION|</th></tr>';
    foreach ($result as $row) {
        echo '<tr><td>'.$row->firstname.'</td><td>'.$row->lastname.'</td><td>'.intval($row->note_lire).'</td><td>'.intval($row->note_ecouter).'</td>
                  <td>'.intval($row->note_ecrire).'</td><td>'.intval($row->note_conversation).'</td><td>'.intval($row->note_expression).'</td>
        </tr>';
    }
    echo '</table>';
} else {
    echo 'Aucun résultat trouvé.';
}
echo '</br></br>'.intval($row->note_lire).'</br></br></br></br>';
*/
?>
<!DOCTYPE html>
<html>

    <head >
        <title style="margin-top: 100px">Attestation de compétences</title>
    </head>
    <body style="margin-left: 40px ">
        
    <div style="float:left">
    <h3>Notre devise, faire mieux</h3>
    <p style="color:blue"   ><strong>L'humain au cœur de nos actions</strong></p>
</div>
<div style="text-align:center;">
    <h1>ATTESTATION DE COMPÉTENCES</h1>
    <hr>
</div>
<div style="float:left;">
    <p><strong>Nom et Prénom :  </strong>Armanda Lucie</p>
    <p><strong>Projet professionnel : </strong></p>
    <p><strong>Date évaluation initiale : </strong>25/03/2022</p>
    <p><strong>Date évaluation formative : </strong>10/04/2022</p>
    <p><strong>Date évaluation finale : </strong>23/04/2022</p>
    <div >
        </div>
        <table style="border: 1px solid black; float: left;">
            <tr>
                <td><strong>1. Écouter </strong></td>
                <td>
                    <p><strong>A1 :</strong> Peut comprendre des mots quotidiens si les gens parlent lentement et distinctement</p>
                    <p><strong>A2 :</strong> Peut comprendre des mots et des expressions en lien avec l’environnement proche. Peut comprendre l’essentiel d’un message standard</p>
                    <p><strong>B1 :</strong> Peut comprendre une langue orale standard en direct ou à la radio sur des sujets familiers et non familiers en lien avec l’environnement quotidien et professionnel</p></br><hr>
                </td>
            </tr>
            <tr>
            <td><strong>2. Lire </strong></td>
            <td>
               <p><strong>A1 :</strong> Peut comprendre des mots courants et des phrases très simples, par exemple sur des affiches</p>
               <p><strong>A2 :</strong> Peut lire des textes courts très simples. Peut trouver une information simple (horaires…)</p>
               <p><strong>B1 :</strong> Peut comprendre des textes relatifs à son travail</p></br><hr>
            </td>
         </tr>
         <tr>
            <td><strong>3. Prendre part à une conversation  </strong></td>
            <td>
               <p><strong>A1 :</strong> Peut communiquer de façon simple si l'interlocuteur répète ou reformule ses phrases. Peut poser des questions simples</p>
               <p><strong>A2 :</strong> Peut utiliser des phrases simples pour décrire son activité professionnelle ou quotidienne</p>
               <p><strong>B1 :</strong> Peut comprendre sans préparation une conversation sur des sujets connus</p></br><hr>
            </td>
         </tr>
         <tr>
             <td><strong>4. S’exprimer oralement en continu  .</strong></td>
             <td>
                 <p><strong>A1 :</strong> Peut utiliser des expressions et des phrases simples</p>
                 <p><strong>A2 :</strong> Peut utiliser une série de phrases ou d’expressions pour décrire en termes simples son environnement proche</p>
                 <p><strong>B1 :</strong> Peut comprendre sans préparation une conversation sur des sujets connus</p></br><hr>
                </td>            
            </tr>
        <td><strong>5. Ecrire  </strong></td>
        <td>
               <p><strong>A1 :</strong> Peut comprendre des mots courants et des phrases très simples, par exemple sur des affiches</p>
               <p><strong>A2 :</strong> Peut lire des textes courts très simples. Peut trouver une information simple (horaires…)</p>
               <p><strong>B1 :</strong> Peut comprendre des textes relatifs à son travail</p>
            </td>
        </tr>
    </table>
	<div class='graph'>

		<?php
		echo "<img src= './radar1.php' />"
		?>
	</div>
</div>


</body>
<footer>
    
    <p>
        Est potest nisi facile mihi quae dirimi ea amoris natura res inter sumus illa res non nacti multo et videamur animadverti natos detestabili in evidentius a et nacti sensus multo indigentia ex virtutis eo sit orta natura in res ex inter nisi sensus mihi ea amicitia esset ut magis sensus.
        
    </p>
    
</footer>
</html>
<!--
    <!DOCTYPE html>
    <html>
        <head>
            <title>Attestation de compétences</title>
        </head>
        <body>
            <div style="float:left">
                <p>Notre devise, faire mieux</p>
                <p>NOM et Prénom : Armanda Lucie</p>
                <p>Projet professionnel :</p>
                <p>Date évaluation initiale : 25/03/2022</p>
                <p>Date évaluation formative : 10/04/2022</p>
		<p>Date évaluation finale : 23/04/2022</p>
	</div>
	<div style="margin-left:200px">
        <h1>ATTESTATION DE COMPÉTENCES</h1>
		<hr>
		<table>
            <tr>
                <td>1. Écouter</td>
				<td>
                    <p>A1 : Peut comprendre des mots quotidiens si les gens parlent lentement et distinctement</p>
					<p>A2 : Peut comprendre des mots et des expressions en lien avec l’environnement proche. Peut comprendre l’essentiel d’un message standard</p>
					<p>B1: Peut comprendre une langue orale standard en direct ou à la radio sur des sujets familiers et non familiers en lien avec l’environnement quotidien et professionnel</p>
				</td>
			</tr>
			<tr>
				<td>2. Lire</td>
				<td>
					<p>A1 : Peut comprendre des mots courants et des phrases très simples, par exemple sur des affiches</p>
					<p>A2 : Peut lire des textes courts très simples. Peut trouver une information simple (horaires…)</p>
					<p>B1 : Peut comprendre des textes relatifs à son travail</p>
				</td>
			</tr>
			<tr>
				<td>3. Prendre part à une conversation</td>
				<td>
					<p>A1 : Peut communiquer de façon simple si l'interlocuteur répète ou reformule ses phrases. Peut poser des questions simples</p>
					<p>A2 : Peut utiliser des phrases simples pour décrire son activité professionnelle ou quotidienne</p>
					<p>B1 : Peut comprendre sans préparation une conversation sur des sujets connus</p>
				</td>
			</tr>
			<tr>
				<td>4. S’exprimer oralement en continu</td>
				<td>
					<p>A1 : Peut utiliser des expressions et des phrases simples</p>
					<p>A2 : Peut utiliser une série de phrases ou d’expressions pour décrire en termes simples son environnement proche</p>
					<p>B1 : Peut articuler des expressions pour raconter des évènements, des projets et exprimer ses réactions.</p>
				</td>
			</tr>
			<tr>
				<td>5. Expression écrite</td>
				<td>
					<p>A1 : Peut écrire son nom, sa nationalité sur une fiche, des mots simples</p>
					<p>A2 : Peut écrire des messages simples</p>
					<p>B1 : Peut articuler des expressions pour raconter des évènements, des projets et exprimer ses réactions.</p>
				</td>
			</tr>

</body>