<?php

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\IReader;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;


require_once('../../config.php');
require_once($CFG->dirroot . '/local/mr/bootstrap.php');
require_once($CFG->libdir . '/gradelib.php');
require_once($CFG->dirroot . '/local/mr/PhpSpreadsheet/src/PhpSpreadsheet/IOFactory.php');

require_once($CFG->dirroot . '/local/mr/jpgraph/src/jpgraph.php');
require_once($CFG->dirroot . '/local/mr/jpgraph/src/jpgraph_radar.php');


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


// ID du cours
$courseid = 6;

// Nom du fichier Excel contenant les notes
$filename = 'graph1 Note.xlsx';


// Lecture du fichier Excel
$reader = IOFactory::createReader('Xlsx');
$spreadsheet = $reader->load("chemin/vers/le/fichier.xlsx");

// Accès à la feuille de calcul
$worksheet = $spreadsheet->getActiveSheet();

// Accès aux cellules
$cellValue = $worksheet->getCell('A1')->getValue();

// Titres à utiliser pour chaque valeur
$titles = array('ecouter', 'lire', 'conversation', 'expression', 'ecrire', 'Valeur 6');

// Créer un objet graphique de 800 x 800 pixels
$graph = new RadarGraph(800, 800);

// Définir l'échelle de l'axe des x
$graph->SetScale("lin", 0, 100);

//couleur du fond du graph
$graph->SetColor(array(50,150,100));
$graph->SetTitles($titles);

// Créer un objet radarplot avec les données
$data = array();
foreach ($grades as $userid => $grade) {
    $data[] = $grade;
}
$radarplot = new RadarPlot($data);

// Ajouter le radarplot au graphique
$graph->Add($radarplot);

// Ajouter une légende
$graph->legend->Pos(0.1, 0.1);

// Afficher le graphique
$graph->Stroke();
