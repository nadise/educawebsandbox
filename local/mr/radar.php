<?php
require_once('../../config.php');
require_once($CFG->dirroot . '/local/mr/bootstrap.php');
require_once($CFG->dirroot . '/local/mr/jpgraph/src/jpgraph.php');
require_once($CFG->dirroot . '/local/mr/jpgraph/src/jpgraph_radar.php');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


// Données à utiliser dans le graphique
$data = array(85, 45, 95, 10, 50  );


// Titres à utiliser pour chaque valeur
$titles = array('ecouter', 'lire', 'conversation', 'expression', 'ecrire', 'Valeur 6');


// Créer un objet graphique de 400 x 250 pixels
$graph = new RadarGraph(400, 250);



// Définir l'échelle de l'axe des x
$graph->SetScale("lin", 0, 100);

//couleur du fond du graph

$graph->SetColor(array(50,150,100));
$graph->SetTitles($titles);

// Créer un objet radarplot avec les données
$radarplot = new RadarPlot($data);

// Ajouter le radarplot au graphique
$graph->Add($radarplot);

// Ajouter une légende
$graph->legend->Pos(0.1, 0.1);

// Afficher le graphique
$graph->Stroke();
echo 'coucou';
