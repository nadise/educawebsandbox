<?php

require_once('../../config.php');
require_once($CFG->dirroot . '/local/mr/bootstrap.php');
require_once($CFG->dirroot . '/local/mr/jpgraph/src/jpgraph.php');
require_once($CFG->dirroot . '/local/mr/jpgraph/src/jpgraph_radar.php');
require_once($CFG->dirroot . '/local/mr/PhpSpreadsheet/src/PhpSpreadsheet/IOFactory.php');


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


$courseid = 6;
$bibi = 'coucou';
echo $bibi;
// Récupérer le contexte Moodle
$context = context::instance_by_id($courseid);

// Construire le chemin du fichier en utilisant l'ID du cours
$filename = $CFG->dataroot . '/courses/' . $context->instanceid . '/graph1.xlsx';
$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('xlsx');

/*
$reader->setReadDataOnly(true);

$worksheet = $reader->load($filename)->getActiveSheet();

$dataArray = array();

foreach ($worksheet->getRowIterator() as $row) {
    $rowData = array();
    $cellIterator = $row->getCellIterator();
    $cellIterator->setIterateOnlyExistingCells(FALSE); 

    foreach ($cellIterator as $cell) {
        $rowData[] = $cell->getValue();
    }

    $dataArray[] = $rowData;
}

foreach ($dataArray as $row) {
  // Récupérer les données du stagiaire
  $nom = $row[0];
  $noteA = $row[1];
  $noteB = $row[2];
  $noteC = $row[3];
  $noteD = $row[4];

  // Créer un tableau de données pour le graphique radar
  $data = array($noteA, $noteB, $noteC, $noteD);

  // Créer un nouveau graphique radar
  $graph = new RadarGraph(400, 300);

  // Ajouter les données au graphique radar
  $plot = new RadarPlot($data);
  $graph->Add($plot);

  // Définir les titres des axes
  $graph->SetTitles(array('Compétence A', 'Compétence B', 'Compétence C', 'Compétence D'));

  // Définir le titre du graphique radar
  $graph->title->Set('Résultats de '.$nom);

  // Afficher le graphique radar
  $graph->Stroke();
}




/*
php



require_once($CFG->dirroot . '/local/exportnotes/PhpSpreadsheet/src/PhpSpreadsheet/IOFactory.php');

use PhpOffice\PhpSpreadsheet\IOFactory;


// Récupérer le contexte Moodle
$context = context::instance_by_id($courseid);
//plutot que mettre : $filename = 'chemin/vers/fichier/exporté.xlsx'; 
// Construire le chemin du fichier en utilisant l'ID du cours
$filename = $CFG->dirroot . '/grade/export/xls/index.php?id=' . $context->instanceid;



$spreadsheet = IOFactory::load($inputFileName);
$data = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);


// Inclure la bibliothèque PHPExcel
require_once($CFG->dirroot . '/local/exportnotes/PHPExcel/PHPExcel.php');

// Lire les données du fichier Excel
$filename = $CFG->dirroot . '/local/exportnotes/notes_eval.xlsx';

$reader = IOFactory::createReader('Xlsx'); // Remplace PHPExcel_IOFactory::createReaderForFile
$spreadsheet = $reader->load($filename); // Charge le fichier de feuille de calcul

//$reader = PHPExcel_IOFactory::createReaderForFile($filename);
$reader->setReadDataOnly(true);

$worksheet = $reader->load($filename)->getActiveSheet();

$data = array();

foreach ($worksheet->getRowIterator() as $row) {
    $rowData = array();
    $cellIterator = $row->getCellIterator();
    $cellIterator->setIterateOnlyExistingCells(FALSE); 

    foreach ($cellIterator as $cell) {
        $rowData[] = $cell->getValue();
    }

    $data[] = $rowData;
}

// Boucler à travers chaque ligne du fichier Excel
foreach ($data as $row) {
  // Récupérer les données du stagiaire
  $nom = $row[0];
  $noteA = $row[1];
  $noteB = $row[2];
  $noteC = $row[3];
  $noteD = $row[4];
  
  // Créer un tableau de données pour le graphique radar
  $data = array($noteA, $noteB, $noteC, $noteD);
  
  // Créer un nouveau graphique radar
  $graph = new RadarGraph(400, 300);
  
  // Définir les titres des axes
  $graph->SetTitles(array('Compétence A', 'Compétence B', 'Compétence C', 'Compétence D'));
  
  // Ajouter les données au graphique radar
  $plot = new RadarPlot($data);
  $graph->Add($plot);
  
  // Définir le titre du graphique radar
  $graph->title->Set('Résultats de '.$nom);
  
  // Afficher le graphique radar
  $graph->Stroke();

  $filename = 'radar_'.$nom.'.png';
  $graph->Stroke($filename);

  //l'endroit ou il est stocké :

  $graph->Stroke('/var/www/word_export/'.$nom.'.png');

}

?>*/